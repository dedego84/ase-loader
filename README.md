# ASE Loader

A webpack loader to convert Adobe Swatch Exchange files into CSS variables.
The imported `.ase` files should be passed to both CSS-loader and Style-loader
for it to function as intended.

### Example

`webpack.conf.js`
```javascript
    module: {
        rules: [
            {
                test: /\.ase$/,
                use: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'ase-loader',
                        options: {
                            variations: 5, // How many light and dark variations of a single swatch color
                                           // Any number between 1 and 20
                            step: 2        // How big the steps are for the lighter and darker variations
                                           // A number between 1 and 99
                        }
                    },
                ]
            }
        ]
    },
```

`lit.html.component.js`
```javascript
    import 'somePath/seawaves.ase'

    // or with a given name for the theme
    import 'somePath/seawaves.ase?as=appTheme'
```

The name is extracted from the `.ase` file by default, but can be customized by adding 
the query parameter `as` to specify the name for the output. The example below gives an
example of that.

```css
:root {
    --color-seawaves-0-l4: #025D8A;
    --color-seawaves-0-l2: #02547D;
    --color-seawaves-0-ori: #02547D;
    --color-seawaves-0-d2: #02547D;
    --color-seawaves-0-d4: #024B70;
}

/** or with a given name for the theme **/
:root {
    --color-apptheme-0-l4: #025D8A;
    --color-apptheme-0-l2: #02547D;
    --color-apptheme-0-ori: #02547D;
    --color-apptheme-0-d2: #02547D;
    --color-apptheme-0-d4: #024B70;
}
```

As you can see the names will always be lowercase and all non alphanumerical characters are stripped.
The original colors taken from the swatch file will always be marked with `-ori` at the end.