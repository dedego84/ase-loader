const Color = require('color');
const ReadASE = require('readase');
const fs = require('fs');
const loaderUtils = require('loader-utils');

const has = (obj, key, upper, lower) => obj 
    && obj[key]
    && typeof obj[key] === 'number'
    && obj[key] < upper
    && obj[key] > lower;

module.exports = function() {
    this.cacheable();

    let writeToCssOutput;   
    const callback = this.async();
    const options = loaderUtils.getOptions(this); 
    const params = this.resourceQuery.indexOf('?') > -1 ? loaderUtils.parseQuery(this.resourceQuery) : {};
    const resource = this.resource.split('?')[0];
    const hasVariations = has(options, 'variations', 21, 0);
    const hasstep = has(options, 'step', 100, 0);
    const variations = hasVariations ? options.variations : 3;
    const step = hasstep ? Math.floor(options.step) : 10;

    fs.readFile(resource, (error, buffer) => {
        if (!error) {
            const {entries, name: aseName} = ReadASE(buffer)[0];
            // If a name is specified (with the property "as") in the query parameters of the file request
            // Then this name will be used as the key name for the CSS variables, which makes theming eassier.
            const name = (params && params.as && params.as !== '' ? params.as : aseName).toLowerCase().replace(/[^a-zA-Z\d]/g, '');
            
            writeToCssOutput = ':root {\n';
            entries.forEach(function(entry, index) {
                const origin = Color( entry.color.hex );
                const originKey = `--color-${name}-${index}`;
                const light = [], dark = [];
                for(let i = 0; i < variations; i++) {
                    light.push(`${originKey}-l${(i+1)*step}: ${origin.lighten((i+(step/100))/10).hex()};\n`);
                    dark.push(`${originKey}-d${(i+1)*step}: ${origin.darken((i+(step/100))/10).hex()};\n`)
                }   

                writeToCssOutput = `
                    ${writeToCssOutput}
                    ${light.reverse().join('')}\n
                    ${originKey}-ori: ${origin.hex()};\n
                    ${dark.join('')}\n
                `;

            });
            writeToCssOutput = `${writeToCssOutput}}`;
            callback(null, writeToCssOutput);
        } else {
            callback(error);
        }
    });
}